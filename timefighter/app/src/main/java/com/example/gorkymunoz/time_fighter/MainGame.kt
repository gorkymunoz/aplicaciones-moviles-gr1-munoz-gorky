package com.example.gorkymunoz.time_fighter

import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.navArgs
import com.evernote.android.state.State
import com.evernote.android.state.StateSaver
import com.google.firebase.firestore.FirebaseFirestore


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [MainGame.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [MainGame.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class MainGame : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    //private var listener: OnFragmentInteractionListener? = null

    internal lateinit var welcomeMessage: TextView
    internal lateinit var gameScoreTextView: TextView
    internal lateinit var timeLeftTextView: TextView
    internal lateinit var goButton: Button

    val db = FirebaseFirestore.getInstance()

    @State
    var score = 0
    @State
    var gameStarted = false
    @State
    var timeLeft = 10


    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000
    private lateinit var countDownTimer: CountDownTimer //implementa el Timer.
    private var playerName = "";


    val args: MainGameArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        StateSaver.restoreInstanceState(this, savedInstanceState)
        gameScoreTextView = view.findViewById(R.id.score)
        goButton = view.findViewById(R.id.go_button)
        timeLeftTextView = view.findViewById(R.id.timeLeft)

        gameScoreTextView.text = getString(R.string.score, score)
        timeLeftTextView.text = getString(R.string.timeLeft, timeLeft)
        /*if(savedInstanceState!= null){
            score = savedInstanceState.getInt(SCORE_KEY)
        }*/

        welcomeMessage = view.findViewById(R.id.welcomeMessage)
        playerName = args.playerName
//        welcomeMessage.text = playerName.toString()
        welcomeMessage.text = getString(R.string.welcome_player, playerName.toString())

        goButton.setOnClickListener{ incrementScore()}

        if(gameStarted) {
            restoreGame()
            return
        }

            resetGame()

    }


    fun incrementScore(){
        if(!gameStarted){
            startGame()
        }
        score++

        gameScoreTextView.text = getString(R.string.score, score)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_game, container, false)
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        StateSaver.saveInstanceState(this, outState)
        //outState.putInt(SCORE_KEY,score)  // se guarda en el score con una llave
        countDownTimer.cancel()
    }

    fun resetGame(){
        score = 0
        gameScoreTextView.text = getString(R.string.score, score)

        timeLeft = 10
        timeLeftTextView.text = getString(R.string.timeLeft, timeLeft)

        countDownTimer = object  : CountDownTimer(initialCountDown, countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.timeLeft, timeLeft)
            }

            override fun onFinish() {
                endGame()
                timeLeft = 10
                timeLeftTextView.text = getString(R.string.timeLeft, timeLeft)
                gameStarted = false
                score = 0
                gameScoreTextView.text = getString(R.string.score, score)
            }

        }

        gameStarted = false

    }

    fun startGame(){
        countDownTimer.start()
        gameStarted = true
    }

    private fun restoreGame() {
        gameScoreTextView.text = getString(R.string.score, score)

        countDownTimer = object : CountDownTimer(timeLeft * 1000L, countDownInterval) {
            override fun onFinish() {
                endGame()
                timeLeft = 10
                timeLeftTextView.text = getString(R.string.timeLeft, timeLeft)
                gameStarted = false
                score = 0
                gameScoreTextView.text = getString(R.string.score, score)
            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.timeLeft, timeLeft)
            }

        }

        countDownTimer.start()
    }

    fun endGame(){
        Toast.makeText(activity, resources.getString(R.string.score, score), Toast.LENGTH_LONG).show()


        //Guardar en base
        val data = HashMap<String,Any>()
        data["name"] = playerName
        data["score"] = score
        data.put("birth",1984)

        db.collection("players")
            .add(data)
            .addOnSuccessListener { documentReference ->
                Log.e("MAIN_GAME","documentSnapshot added with ID: ${documentReference.id}")
            }
            .addOnFailureListener{e-> Log.e("MAIN_GAME","Error adding document",e)}

        resetGame()
    }

}
