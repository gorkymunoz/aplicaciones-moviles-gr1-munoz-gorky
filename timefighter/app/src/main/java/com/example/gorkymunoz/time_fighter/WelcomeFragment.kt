package com.example.gorkymunoz.time_fighter

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.findNavController


class WelcomeFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var listener: OnFragmentInteractionListener? = null

    internal lateinit var startButton: Button
    internal lateinit var playerName: TextView
    internal lateinit var scoresButton: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        playerName = view.findViewById(R.id.player_name)
        startButton = view.findViewById(R.id.start_button)
        scoresButton = view.findViewById(R.id.scores_button)
        startButton.setOnClickListener{goToMainGame()}
        scoresButton.setOnClickListener { goToScores() }
    }

    fun goToMainGame(){
        var action = WelcomeFragmentDirections.actionMainGame(playerName.text.toString())
        view?.findNavController()?.navigate(action)
    }

    fun goToScores(){
        

    }


















    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_welcome, container, false)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

}
