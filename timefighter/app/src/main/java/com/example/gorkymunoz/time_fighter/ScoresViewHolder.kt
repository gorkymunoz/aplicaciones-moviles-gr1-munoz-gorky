package com.example.gorkymunoz.time_fighter

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ScoresViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {

    var playerName = itemView.findViewById<TextView>(R.id.tvViewHolderName)
    var playerScore = itemView.findViewById<TextView>(R.id.tvViewHolderScore)

}